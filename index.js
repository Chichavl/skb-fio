var express = require('express')
var app = express()

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

function notLetters(t)
{
    var regex = /_|[0-9]|\.|\\|\/|,|!|@|#|\$|%|\^|&|\*|\(|\)|\+|-|;/; ///\d/g;
    return regex.test(t);
}

function toTitleCase(str)
{
    return str.substring(0, 1).toUpperCase() + str.substring(1).toLowerCase();
}


app.get('/', function (req, res) {
    var fio = req.query.fullname.replace(/\s+/g,' ').trim();;
    var fioArr = fio.split(" ");
    var ans;
    console.log(fio);
    console.log(fioArr);
    if (fioArr.length == 1 && fioArr[0] != "" && !notLetters(fioArr[0])) {
        ans = toTitleCase(fioArr[0]);
        console.log(ans);
        res.send(ans);
    }
    else if (fioArr.length == 2 && !notLetters(fioArr[0] + fioArr[1])) {
        ans = toTitleCase(fioArr[1]);
        ans += " ";
        ans += fioArr[0].charAt(0).toUpperCase();
        ans += ".";
        console.log(ans);
        res.send(ans);
    }
    else if (fioArr.length == 3 && !notLetters(fioArr[0] + fioArr[1] + fioArr[2])) {
        ans = toTitleCase(fioArr[2]);
        ans += " ";
        ans += fioArr[0].charAt(0).toUpperCase();
        ans += ".";

        ans += " ";
        ans += fioArr[1].charAt(0).toUpperCase();
        ans += ".";
        console.log(ans);
        res.send(ans);
    }
    else {
        console.log(ans);
        res.send("Invalid fullname");
    }
});

app.listen(3000, function () {
    console.log('Example app listening on port 3000!')
});